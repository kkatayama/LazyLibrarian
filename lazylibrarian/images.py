#  This file is part of Lazylibrarian.
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.


import os
import traceback
import subprocess
import json

import lazylibrarian
from lazylibrarian import logger, database
from lazylibrarian.bookwork import getBookWork, NEW_WHATWORK
from lazylibrarian.formatter import plural, makeUnicode, makeBytestr, safe_unicode, check_int, makeUTF8bytes
from lazylibrarian.common import safe_copy, setperm, path_isfile, syspath, jpg_file
from lazylibrarian.cache import cache_img, fetchURL
from lazylibrarian.providers import ProviderIsBlocked, BlockProvider
from six import PY2, text_type
# noinspection PyUnresolvedReferences
from six.moves.urllib_parse import quote_plus
from shutil import rmtree

try:
    import zipfile
except ImportError:
    if PY2:
        import lib.zipfile as zipfile
    else:
        import lib3.zipfile as zipfile

try:
    import PIL
    # noinspection PyUnresolvedReferences
    from PIL import Image as PILImage
    from icrawler.builtin import GoogleImageCrawler
except ImportError:
    PIL = None
    GoogleImageCrawler = None

try:
    # noinspection PyProtectedMember
    from PyPDF3 import PdfFileWriter, PdfFileReader
except ImportError:
    try:
        # noinspection PyProtectedMember
        from lib.PyPDF3 import PdfFileWriter, PdfFileReader
    except ImportError:
        PdfFileWriter = None
        PdfFileReader = None

GS = ''
GS_VER = ''
generator = ''


def createthumb(jpeg):
    if not PIL:
        return ''

    basewidth = 300
    img = PILImage.open(jpeg)
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    # noinspection PyUnresolvedReferences
    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    fname, extn = os.path.splitext(jpeg)
    outfile = fname + '_thumb' + extn
    img.save(outfile)
    return outfile


def coverswap(sourcefile):
    if PdfFileWriter is None:
        logger.warn("PyPDF3 is not loaded")
        return False

    _, extn = os.path.splitext(sourcefile)
    if extn.lower() != '.pdf':
        logger.warn("Cannot swap cover on [%s]" % sourcefile)
        return False
    try:
        # reordering pages is quite slow if the source is on a networked drive
        # so work on a local copy, then move it over.
        original = sourcefile
        logger.debug("Copying %s" % original)
        srcfile = safe_copy(original, os.path.join(lazylibrarian.CACHEDIR, os.path.basename(sourcefile)))
        output = PdfFileWriter()
        with open(srcfile, "rb") as f:
            input1 = PdfFileReader(f)
            cnt = input1.getNumPages()
            logger.debug("Found %s pages in %s" % (cnt, srcfile))
            output.addPage(input1.getPage(1))
            # logger.debug("Added page 1")
            output.addPage(input1.getPage(0))
            # logger.debug("Added page 0")
            p = 2
            while p < cnt:
                output.addPage(input1.getPage(p))
                # logger.debug("Added page %s" % p)
                p = p + 1
            with open(srcfile + 'new', "wb") as outputStream:
                output.write(outputStream)
        logger.debug("Writing new output file")
        try:
            newcopy = safe_copy(srcfile + 'new', original + 'new')
        except Exception as e:
            logger.warn("Failed to copy output file: %s" % str(e))
            return False
        os.remove(srcfile)
        os.remove(srcfile + 'new')
        # windows does not allow rename to overwrite an existing file
        os.remove(original)
        os.rename(newcopy, original)
        logger.info("%s has %d pages. Swapped pages 1 and 2\n" % (sourcefile, cnt))
        return True

    except Exception as e:
        logger.warn(str(e))
        return False


def getAuthorImages():
    """ Try to get an author image for all authors without one"""
    myDB = database.DBConnection()
    cmd = 'select AuthorID, AuthorName from authors where (AuthorImg like "%nophoto%" or AuthorImg is null)'
    cmd += ' and Manual is not "1"'
    authors = myDB.select(cmd)
    if authors:
        logger.info('Checking images for %s %s' % (len(authors), plural(len(authors), "author")))
        counter = 0
        for author in authors:
            authorid = author['AuthorID']
            imagelink = getAuthorImage(authorid)
            newValueDict = {}
            if not imagelink:
                logger.debug('No image found for %s' % author['AuthorName'])
                newValueDict = {"AuthorImg": 'images/nophoto.png'}
            elif 'nophoto' not in imagelink:
                logger.debug('Updating %s image to %s' % (author['AuthorName'], imagelink))
                newValueDict = {"AuthorImg": imagelink}

            if newValueDict:
                counter += 1
                controlValueDict = {"AuthorID": authorid}
                myDB.upsert("authors", newValueDict, controlValueDict)

        msg = 'Updated %s %s' % (counter, plural(counter, "image"))
        logger.info('Author Image check complete: ' + msg)
    else:
        msg = 'No missing author images'
        logger.debug(msg)
    return msg


def getBookCovers():
    """ Try to get a cover image for all books """

    myDB = database.DBConnection()
    cmd = 'select BookID,BookImg from books where BookImg like "%nocover%" '
    cmd += 'or BookImg like "%nophoto%" and Manual is not "1"'
    books = myDB.select(cmd)
    if books:
        logger.info('Checking covers for %s %s' % (len(books), plural(len(books), "book")))
        counter = 0
        for book in books:
            bookid = book['BookID']
            coverlink, _ = getBookCover(bookid)
            if coverlink and "nocover" not in coverlink and "nophoto" not in coverlink:
                controlValueDict = {"BookID": bookid}
                newValueDict = {"BookImg": coverlink}
                myDB.upsert("books", newValueDict, controlValueDict)
                counter += 1
            if not coverlink and "http" in book['BookImg']:
                controlValueDict = {"BookID": bookid}
                newValueDict = {"BookImg": "images/nocover.png"}
                myDB.upsert("books", newValueDict, controlValueDict)
        msg = 'Updated %s %s' % (counter, plural(counter, "cover"))
        logger.info('Cover check complete: ' + msg)
    else:
        msg = 'No missing book covers'
        logger.debug(msg)
    return msg


def getBookCover(bookID=None, src=None):
    """ Return link to a local file containing a book cover image for a bookid, and which source used.
        Try 1. Local file cached from goodreads/googlebooks when book was imported
            2. cover.jpg if we have the book
            3. LibraryThing cover image (if you have a dev key)
            4. LibraryThing whatwork (if available)
            5. Goodreads search (if book was imported from goodreads)
            6. OpenLibrary image
            7. Google isbn search (if google has a link to book for sale)
            8. Google images search (if lazylibrarian config allows)

        src = cache, cover, goodreads, librarything, whatwork, googleisbn, openlibrary, googleimage
        Return None if no cover available. """
    if not bookID:
        logger.error("getBookCover- No bookID")
        return None, src

    if not src:
        src = ''
    logger.debug("Getting %s cover for %s" % (src, bookID))
    # noinspection PyBroadException
    try:
        myDB = database.DBConnection()
        cachedir = lazylibrarian.CACHEDIR
        item = myDB.match('select BookImg from books where bookID=?', (bookID,))
        if item:
            coverlink = item['BookImg']
            coverfile = os.path.join(cachedir, "book", item['BookImg'].replace('cache/', ''))
            if coverlink.startswith('http') and 'nocover' in coverlink or 'nophoto' in coverlink:
                coverfile = os.path.join(lazylibrarian.DATADIR, 'images', 'nocover.png')
                coverlink = 'images/nocover.png'
                myDB.action("UPDATE books SET BookImg=? WHERE BookID=?", (coverlink, bookID))
        else:
            coverlink = "cache/book/" + bookID + ".jpg"
            coverfile = os.path.join(cachedir, "book", bookID + '.jpg')
        if not src or src == 'cache' or src == 'current':
            if path_isfile(coverfile):  # use cached image if there is one
                lazylibrarian.CACHE_HIT = int(lazylibrarian.CACHE_HIT) + 1
                return coverlink, 'cache'
            elif src:
                lazylibrarian.CACHE_MISS = int(lazylibrarian.CACHE_MISS) + 1
                return None, src

        if not src or src == 'cover':
            item = myDB.match('select BookFile from books where bookID=?', (bookID,))
            if item:
                bookfile = item['BookFile']
                if bookfile and path_isfile(bookfile):  # we may have a cover.jpg in the same folder
                    bookdir = os.path.dirname(bookfile)
                    coverimg = jpg_file(bookdir)
                    if coverimg:
                        if src:
                            extn = '_cover.jpg'
                        else:
                            extn = '.jpg'

                        coverfile = os.path.join(cachedir, "book", bookID + extn)
                        coverlink = 'cache/book/' + bookID + extn
                        logger.debug("Caching %s for %s" % (extn, bookID))
                        _ = safe_copy(coverimg, coverfile)
                        return coverlink, src
                    else:
                        logger.debug('No cover found for %s in %s' % (bookID, bookdir))
                else:
                    if bookfile:
                        logger.debug("File %s not found" % bookfile)
                    else:
                        logger.debug("No bookfile for %s" % bookID)
            else:
                logger.debug("BookID %s not found" % bookID)
            if src:
                return None, src

        # see if librarything  has a cover
        if not src or src == 'librarything':
            if lazylibrarian.CONFIG['LT_DEVKEY']:
                cmd = 'select BookISBN from books where bookID=?'
                item = myDB.match(cmd, (bookID,))
                if item and item['BookISBN']:
                    img = '/'.join([lazylibrarian.CONFIG['LT_URL'], 'devkey/%s/large/isbn/%s' % (
                           lazylibrarian.CONFIG['LT_DEVKEY'], item['BookISBN'])])
                    if src:
                        coverlink, success, _ = cache_img("book", bookID + '_lt', img)
                    else:
                        coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                    # if librarything has no image they return a 1x1 gif
                    data = ''
                    coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                    if path_isfile(coverfile):
                        with open(syspath(coverfile), 'rb') as f:
                            data = f.read()
                    if len(data) < 50:
                        logger.debug('Got an empty librarything image for %s [%s]' % (bookID, img))
                    elif success:
                        logger.debug("Caching librarything cover for %s" % bookID)
                        return coverlink, 'librarything'
                    else:
                        logger.debug('Failed to cache image for %s [%s]' % (img, coverlink))
                else:
                    logger.debug("No isbn for %s" % bookID)
            if src:
                return None, src

        # see if librarything workpage has a cover
        if NEW_WHATWORK and (not src or src == 'whatwork'):
            work = getBookWork(bookID, "Cover")
            if work:
                try:
                    img = work.split('workCoverImage')[1].split('="')[1].split('"')[0]
                    if img and img.startswith('http'):
                        if src:
                            coverlink, success, _ = cache_img("book", bookID + '_ww', img)
                        else:
                            coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                        # if librarything has no image they return a 1x1 gif
                        data = ''
                        coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                        if path_isfile(coverfile):
                            with open(syspath(coverfile), 'rb') as f:
                                data = f.read()
                        if len(data) < 50:
                            logger.debug('Got an empty whatwork image for %s [%s]' % (bookID, coverlink))
                        elif success:
                            logger.debug("Caching whatwork cover for %s" % bookID)
                            return coverlink, 'whatwork'
                        else:
                            logger.debug('Failed to cache image for %s [%s]' % (img, coverlink))
                    else:
                        logger.debug("No image found in work page for %s" % bookID)
                except IndexError:
                    logger.debug('workCoverImage not found in work page for %s' % bookID)

                try:
                    img = work.split('og:image')[1].split('="')[1].split('"')[0]
                    if img and img.startswith('http'):
                        if src:
                            coverlink, success, _ = cache_img("book", bookID + '_ww', img)
                        else:
                            coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                        # if librarything has no image they return a 1x1 gif
                        data = ''
                        coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                        if path_isfile(coverfile):
                            with open(syspath(coverfile), 'rb') as f:
                                data = f.read()
                        if len(data) < 50:
                            logger.debug('Got an empty whatwork image for %s [%s]' % (bookID, coverlink))
                        if success:
                            logger.debug("Caching whatwork cover for %s" % bookID)
                            return coverlink, 'whatwork'
                        else:
                            logger.debug('Failed to cache image for %s [%s]' % (img, coverlink))
                    else:
                        logger.debug("No image found in work page for %s" % bookID)
                except IndexError:
                    logger.debug('og:image not found in work page for %s' % bookID)
            else:
                logger.debug('No work page for %s' % bookID)
            if src:
                return None, src

        cmd = 'select BookName,AuthorName,BookLink,BookISBN from books,authors where bookID=?'
        cmd += ' and books.AuthorID = authors.AuthorID'
        item = myDB.match(cmd, (bookID,))
        safeparams = ''
        booklink = ''
        if item:
            title = safe_unicode(item['BookName'])
            author = safe_unicode(item['AuthorName'])
            booklink = item['BookLink']
            safeparams = quote_plus(makeUTF8bytes("%s %s" % (author, title))[0])

        # try to get a cover from goodreads
        if not src or src == 'goodreads':
            if booklink and 'goodreads' in booklink:
                # if the bookID is a goodreads one, we can call https://www.goodreads.com/book/show/{bookID}
                # and scrape the page for og:image
                # <meta property="og:image" content="https://i.gr-assets.com/images/S/photo.goodreads.com/books/
                # 1388267702i/16304._UY475_SS475_.jpg"/>
                # to get the cover
                result, success = fetchURL(booklink)
                if success:
                    try:
                        img = result.split('id="coverImage"')[1].split('src="')[1].split('"')[0]
                    except IndexError:
                        try:
                            img = result.split('og:image')[1].split('="')[1].split('"')[0]
                        except IndexError:
                            img = None
                    if img and img.startswith('http') and 'nocover' not in img and 'nophoto' not in img:
                        if src == 'goodreads':
                            coverlink, success, _ = cache_img("book", bookID + '_gr', img)
                        else:
                            coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                        data = ''
                        coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                        if path_isfile(coverfile):
                            with open(syspath(coverfile), 'rb') as f:
                                data = f.read()
                        if len(data) < 50:
                            logger.debug('Got an empty goodreads image for %s [%s]' % (bookID, coverlink))
                        elif success:
                            logger.debug("Caching goodreads cover for %s %s" % (item['AuthorName'], item['BookName']))
                            return coverlink, 'goodreads'
                        else:
                            logger.debug("Error getting goodreads image for %s, [%s]" % (img, coverlink))
                    else:
                        logger.debug("No image found in goodreads page for %s" % bookID)
                else:
                    logger.debug("Error getting goodreads page %s, [%s]" % (booklink, result))
            if src:
                return None, src

        # try to get a cover from openlibrary
        if not src or src == 'openlibrary':
            if not ProviderIsBlocked("openlibrary") and item and item['BookISBN']:
                baseurl = '/'.join([lazylibrarian.CONFIG['OL_URL'],
                                   'api/books?format=json&jscmd=data&bibkeys=ISBN:'])
                result, success = fetchURL(baseurl + item['BookISBN'])
                if success:
                    try:
                        source = json.loads(result)  # type: dict
                    except Exception as e:
                        logger.debug("OpenLibrary json error: %s" % e)
                        source = {}

                    img = ''
                    if source:
                        # noinspection PyUnresolvedReferences
                        k = list(source.keys())[0]
                        try:
                            img = source[k]['cover']['medium']
                        except KeyError:
                            try:
                                img = source[k]['cover']['large']
                            except KeyError:
                                logger.debug("No openlibrary image for %s" % item['BookISBN'])

                    if img and img.startswith('http') and 'nocover' not in img and 'nophoto' not in img:
                        if src == 'openlibrary':
                            coverlink, success, _ = cache_img("book", bookID + '_ol', img)
                        else:
                            coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                        data = ''
                        coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                        if path_isfile(coverfile):
                            with open(syspath(coverfile), 'rb') as f:
                                data = f.read()
                        if len(data) < 50:
                            logger.debug('Got an empty openlibrary image for %s [%s]' % (bookID, coverlink))
                        elif success:
                            logger.debug("Caching openlibrary cover for %s %s" % (item['AuthorName'], item['BookName']))
                            return coverlink, 'openlibrary'
                else:
                    logger.debug("OpenLibrary error: %s" % result)
                    BlockProvider("openlibrary", result)
            if src:
                return None, src

        if not src or src == 'googleisbn':
            # try a google isbn page search...
            # there is no image returned if google doesn't have a link for buying the book
            if safeparams:
                URL = "http://www.google.com/search?q=ISBN+" + safeparams
                result, success = fetchURL(URL)
                if success:
                    try:
                        img = result.split('imgurl=')[1].split('&imgrefurl')[0]
                    except IndexError:
                        try:
                            img = result.split('img src="')[1].split('"')[0]
                        except IndexError:
                            img = None

                    if img and img.startswith('http'):
                        if src:
                            coverlink, success, _ = cache_img("book", bookID + '_gi', img)
                        else:
                            coverlink, success, _ = cache_img("book", bookID, img, refresh=True)

                        data = ''
                        coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                        if path_isfile(coverfile):
                            with open(syspath(coverfile), 'rb') as f:
                                data = f.read()
                        if len(data) < 50:
                            logger.debug('Got an empty google image for %s [%s]' % (bookID, coverlink))
                        elif success:
                            logger.debug("Caching google isbn cover for %s %s" %
                                         (item['AuthorName'], item['BookName']))
                            return coverlink, 'google isbn'
                        else:
                            logger.debug("Error caching google image %s, [%s]" % (img, coverlink))
                    else:
                        logger.debug("No image found in google isbn page for %s" % bookID)
                else:
                    logger.debug("Failed to fetch url from google")
            else:
                logger.debug("No parameters for google isbn search for %s" % bookID)
            if src:
                return None, src

        if src == 'googleimage' or not src and lazylibrarian.CONFIG['IMP_GOOGLEIMAGE']:
            if PIL and safeparams:
                icrawlerdir = os.path.join(cachedir, 'icrawler', bookID)
                gc = GoogleImageCrawler(storage={'root_dir': icrawlerdir})
                logger.debug(safeparams)
                gc.crawl(keyword=safeparams, max_num=1)
                if os.path.exists(icrawlerdir):
                    res = len(os.listdir(icrawlerdir))
                else:
                    res = 0
                logger.debug("Found %d %s" % (res, plural(res, 'image')))
                if res:
                    img = os.path.join(icrawlerdir, '000001.jpg')
                    if src:
                        coverlink, success, _ = cache_img("book", bookID + '_gb', img)
                    else:
                        coverlink, success, _ = cache_img("book", bookID, img, refresh=True)
                    data = ''
                    coverfile = os.path.join(lazylibrarian.DATADIR, coverlink)
                    if path_isfile(coverfile):
                        with open(syspath(coverfile), 'rb') as f:
                            data = f.read()
                    if len(data) < 50:
                        logger.debug('Got an empty google search image for %s [%s]' % (bookID, coverlink))
                    elif success:
                        logger.debug("Cached google search cover for %s %s" %
                                     (item['AuthorName'], item['BookName']))
                        rmtree(icrawlerdir, ignore_errors=True)
                        return coverlink, 'google image'
                    else:
                        logger.debug("Error getting google image %s, [%s]" % (img, coverlink))
                else:
                    logger.debug("No images found in google page for %s" % bookID)
                # rmtree(icrawlerdir, ignore_errors=True)
            else:
                if not PIL:
                    logger.debug("PIL not found for google image search for %s" % bookID)
                else:
                    logger.debug("No parameters for google image search for %s" % bookID)
            if src:
                return None, src

        logger.debug("No image found from any configured source")
        return None, src
    except Exception:
        logger.error('Unhandled exception in getBookCover: %s' % traceback.format_exc())
    return None, src


def getAuthorImage(authorid=None, refresh=False, max_num=1):
    if not authorid:
        logger.error("getAuthorImage: No authorid")
        return None

    cachedir = lazylibrarian.CACHEDIR
    coverfile = os.path.join(cachedir, "author", authorid + '.jpg')

    if path_isfile(coverfile) and max_num == 1 and not refresh:  # use cached image if there is one
        lazylibrarian.CACHE_HIT = int(lazylibrarian.CACHE_HIT) + 1
        logger.debug("getAuthorImage: Returning Cached response for %s" % coverfile)
        coverlink = 'cache/author/' + authorid + '.jpg'
        return coverlink

    lazylibrarian.CACHE_MISS = int(lazylibrarian.CACHE_MISS) + 1
    myDB = database.DBConnection()
    author = myDB.match('select AuthorName from authors where AuthorID=?', (authorid,))
    if PIL and author:
        authorname = safe_unicode(author['AuthorName'])
        safeparams = quote_plus(makeUTF8bytes("author %s" % authorname)[0])
        icrawlerdir = os.path.join(cachedir, 'icrawler', authorid)
        rmtree(icrawlerdir, ignore_errors=True)
        gc = GoogleImageCrawler(storage={'root_dir': icrawlerdir})
        gc.crawl(keyword=safeparams, max_num=int(max_num))
        if os.path.exists(icrawlerdir):
            res = len(os.listdir(icrawlerdir))
        else:
            res = 0
        logger.debug("Found %d %s" % (res, plural(res, 'image')))
        if max_num == 1:
            if res:
                img = os.path.join(icrawlerdir, '000001.jpg')
                coverlink, success, was_in_cache = cache_img("author", authorid, img, refresh=refresh)
                if success:
                    if was_in_cache:
                        logger.debug("Returning cached google image for %s" % authorname)
                    else:
                        logger.debug("Cached google image for %s" % authorname)
                    return coverlink
            else:
                logger.debug("No images found for %s" % authorname)
            rmtree(icrawlerdir, ignore_errors=True)
        else:
            return icrawlerdir
    elif not PIL:
        logger.debug("PIL not installed, not looking for author image")
    else:
        logger.debug("No author found for %s" % authorid)
    return None


def createMagCovers(refresh=False):
    if not lazylibrarian.CONFIG['IMP_MAGCOVER']:
        logger.info('Cover creation is disabled in config')
        return ''
    myDB = database.DBConnection()
    #  <> '' ignores empty string or NULL
    issues = myDB.select("SELECT Title,IssueFile from issues WHERE IssueFile <> ''")
    if refresh:
        logger.info("Creating covers for %s %s" % (len(issues), plural(len(issues), "issue")))
    else:
        logger.info("Checking covers for %s %s" % (len(issues), plural(len(issues), "issue")))
    cnt = 0
    for item in issues:
        try:
            maginfo = myDB.match("SELECT CoverPage from magazines WHERE Title=?", (item['Title'],))
            createMagCover(item['IssueFile'], refresh=refresh, pagenum=maginfo['CoverPage'])
            cnt += 1
        except Exception as why:
            logger.warn('Unable to create cover for %s, %s %s' % (item['IssueFile'], type(why).__name__, str(why)))
    logger.info("Cover creation completed")
    if refresh:
        return "Created covers for %s %s" % (cnt, plural(cnt, "issue"))
    return "Checked covers for %s %s" % (cnt, plural(cnt, "issue"))


def find_gs():
    global GS, GS_VER, generator
    if not GS:
        if os.name == 'nt':
            GS = os.path.join(os.getcwd(), "gswin64c.exe")
            generator = "local gswin64c"
            if not path_isfile(GS):
                logger.debug("%s not found" % GS)
                GS = os.path.join(os.getcwd(), "gswin32c.exe")
                generator = "local gswin32c"
            if not path_isfile(GS):
                logger.debug("%s not found" % GS)
                params = ["where", "gswin64c"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = makeUnicode(GS).strip()
                    generator = "gswin64c"
                except Exception as e:
                    logger.debug("where gswin64c failed: %s %s" % (type(e).__name__, str(e)))
                    GS = ''
            if not path_isfile(GS):
                params = ["where", "gswin32c"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = makeUnicode(GS).strip()
                    generator = "gswin32c"
                except Exception as e:
                    logger.debug("where gswin32c failed: %s %s" % (type(e).__name__, str(e)))
            if not path_isfile(GS):
                logger.debug("No gswin found")
                generator = "(no windows ghostscript found)"
                GS = ''
        else:
            GS = os.path.join(os.getcwd(), "gs")
            generator = "local gs"
            if not path_isfile(GS):
                logger.debug("%s not found" % GS)
                GS = ''
                params = ["which", "gs"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = makeUnicode(GS).strip()
                    generator = GS
                except subprocess.CalledProcessError as e:
                    if e.returncode == 1:
                        logger.debug("Could not find gs in your system path")
                    else:
                        logger.debug("which gs failed: %s %s" % (type(e).__name__, str(e)))
            if not path_isfile(GS):
                logger.debug("Cannot find gs")
                generator = "(no gs found)"
                GS = ''
        if GS:
            GS_VER = ''
            # noinspection PyBroadException
            try:
                params = [GS, "--version"]
                res = subprocess.check_output(params, stderr=subprocess.STDOUT)
                res = makeUnicode(res).strip()
                logger.debug("Found %s [%s] version %s" % (generator, GS, res))
                generator = "%s version %s" % (generator, res)
                GS_VER = res
            except Exception as e:
                logger.debug("gs --version failed: %s %s" % (type(e).__name__, str(e)))

    return GS, GS_VER, generator


def shrinkMag(issuefile, dpi=0):
    global GS, GS_VER, generator
    if not issuefile or not path_isfile(issuefile):
        logger.warn('No issuefile %s' % issuefile)
        return ''
    if not GS:
        GS, GS_VER, generator = find_gs()
    if GS_VER:
        outfile = "%s_%s%s" % (issuefile, dpi, '.pdf')
        params = [GS, "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                  "-dCompatibilityLevel=1.3", "-dPDFSETTINGS=/screen",
                  "-dEmbedAllFonts=true", "-dSubsetFonts=true",
                  "-dAutoRotatePages=/None",
                  "-dColorImageDownsampleType=/Bicubic",
                  "-dColorImageResolution=%s" % dpi,
                  "-dGrayImageDownsampleType=/Bicubic",
                  "-dGrayImageResolution=%s" % dpi,
                  "-dMonoImageDownsampleType=/Subsample",
                  "-dMonoImageResolution=%s" % dpi,
                  "-dUseCropBox", "-sOutputFile=%s" % outfile, issuefile]
        try:
            res = subprocess.check_output(params, stderr=subprocess.STDOUT)
            res = makeUnicode(res).strip()
            if not path_isfile(outfile):
                logger.debug("Failed to shrink file: %s" % res)
                return ''
            logger.debug("Resized file: %s" % outfile)
            return outfile
        except Exception as e:
            logger.debug("Failed to shrink file with %s [%s]" % (str(params), e))
            return ''


# noinspection PyUnresolvedReferences
def createMagCover(issuefile=None, refresh=False, pagenum=1):
    global GS, GS_VER, generator
    if not lazylibrarian.CONFIG['IMP_MAGCOVER'] or not pagenum:
        logger.warn('No cover required for %s' % issuefile)
        return ''
    if not issuefile or not path_isfile(issuefile):
        logger.warn('No issuefile %s' % issuefile)
        return ''

    base, extn = os.path.splitext(issuefile)
    if not extn:
        logger.warn('Unable to create cover for %s, no extension?' % issuefile)
        return ''

    coverfile = base + '.jpg'

    if path_isfile(coverfile):
        if refresh:
            os.remove(syspath(coverfile))
        else:
            logger.debug('Cover for %s exists' % issuefile)
            return coverfile  # quit if cover already exists and we didn't want to refresh

    logger.debug('Creating cover for %s' % issuefile)
    data = ''  # result from unzip or unrar
    extn = extn.lower()
    if extn in ['.cbz', '.epub']:
        try:
            data = zipfile.ZipFile(issuefile)
        except Exception as why:
            logger.error("Failed to read zip file %s, %s %s" % (issuefile, type(why).__name__, str(why)))
            data = ''
    elif extn in ['.cbr']:
        if lazylibrarian.UNRARLIB:
            try:
                if lazylibrarian.UNRARLIB == 1:
                    data = lazylibrarian.RARFILE.RarFile(issuefile)
                elif lazylibrarian.UNRARLIB == 2:
                    data = lazylibrarian.RARFILE(issuefile)
            except Exception as why:
                logger.error("Failed to read rar file %s, %s %s" % (issuefile, type(why).__name__, str(why)))
                data = ''
    if data:
        img = None
        try:
            for item in ['cover.j', '000.j', '001.j', '00.j', '01.j']:
                if getattr(data, 'infoiter', None):
                    for member in data.infoiter():
                        if item in member.filename.lower():
                            r = data.read_files(member.filename)
                            img = r[0][1]
                            break
                else:
                    for member in data.namelist():
                        if item in member.lower():
                            img = data.read(member)
                            break
                if img:
                    break
            if img:
                with open(syspath(coverfile), 'wb') as f:
                    f.write(img)
                thumb = createthumb(coverfile)
                if thumb:
                    return thumb
                return coverfile
            else:
                logger.debug("Failed to find image in %s" % issuefile)
        except Exception as why:
            logger.error("Failed to extract image from %s, %s %s" % (issuefile, type(why).__name__, str(why)))

    elif extn == '.pdf':
        if len(lazylibrarian.CONFIG['IMP_CONVERT']):  # allow external convert to override libraries
            generator = "external program: %s" % lazylibrarian.CONFIG['IMP_CONVERT']
            if "gsconvert.py" in lazylibrarian.CONFIG['IMP_CONVERT']:
                msg = "Use of gsconvert.py is deprecated, equivalent functionality is now built in. "
                msg += "Support for gsconvert.py may be removed in a future release. See wiki for details."
                logger.warn(msg)
            converter = lazylibrarian.CONFIG['IMP_CONVERT']
            postfix = ''
            # if not path_isfile(converter):  # full path given, or just program_name?
            #     converter = os.path.join(os.getcwd(), lazylibrarian.CONFIG['IMP_CONVERT'])
            if 'convert' in converter and 'gs' not in converter:
                # tell imagemagick to only convert first page
                postfix = '[0]'
            params = []
            try:
                params = [converter, '%s%s' % (issuefile, postfix), '%s' % coverfile]
                if os.name != 'nt':
                    res = subprocess.check_output(params, preexec_fn=lambda: os.nice(10),
                                                  stderr=subprocess.STDOUT)
                else:
                    res = subprocess.check_output(params, stderr=subprocess.STDOUT)

                res = makeUnicode(res).strip()
                if res:
                    logger.debug('%s reports: %s' % (lazylibrarian.CONFIG['IMP_CONVERT'], res))
            except Exception as e:
                if params:
                    logger.debug(params)
                logger.warn('External "convert" failed %s %s' % (type(e).__name__, str(e)))

        elif os.name == 'nt':
            if not GS:
                GS, GS_VER, generator = find_gs()
            if GS_VER:
                issuefile = issuefile.split('[')[0]
                params = [GS, "-sDEVICE=jpeg", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                          "-dFirstPage=%d" % check_int(pagenum, 1),
                          "-dLastPage=%d" % check_int(pagenum, 1),
                          "-dUseCropBox", "-sOutputFile=%s" % coverfile, issuefile]
                try:
                    res = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    res = makeUnicode(res).strip()
                    if not path_isfile(coverfile):
                        logger.debug("Failed to create jpg: %s" % res)
                except Exception as e:
                    logger.debug("Failed to create cover with %s [%s]" % (str(params), e))
            else:
                logger.warn("Failed to create jpg for %s" % issuefile)
        else:  # not windows
            try:
                # noinspection PyUnresolvedReferences
                from wand.image import Image
                interface = "wand"
            except ImportError:
                try:
                    # No PythonMagick in python3
                    # noinspection PyUnresolvedReferences
                    import PythonMagick
                    interface = "pythonmagick"
                except ImportError:
                    interface = ""
            try:
                if interface == 'wand':
                    generator = "wand interface"
                    with Image(filename=issuefile + '[' + str(check_int(pagenum, 1) - 1) + ']') as img:
                        img.save(filename=coverfile)

                elif interface == 'pythonmagick':
                    generator = "pythonmagick interface"
                    img = PythonMagick.Image()
                    # PythonMagick requires filenames to be bytestr, not unicode
                    if type(issuefile) is text_type:
                        issuefile = makeBytestr(issuefile)
                    if type(coverfile) is text_type:
                        coverfile = makeBytestr(coverfile)
                    img.read(issuefile + '[' + str(check_int(pagenum, 1) - 1) + ']')
                    img.write(coverfile)

                else:
                    if not GS:
                        GS, GS_VER, generator = find_gs()
                    if GS_VER:
                        issuefile = issuefile.split('[')[0]
                        params = [GS, "-sDEVICE=jpeg", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                                  "-dFirstPage=%d" % check_int(pagenum, 1),
                                  "-dLastPage=%d" % check_int(pagenum, 1),
                                  "-dUseCropBox", "-sOutputFile=%s" % coverfile, issuefile]
                        try:
                            res = subprocess.check_output(params, preexec_fn=lambda: os.nice(10),
                                                          stderr=subprocess.STDOUT)
                            res = makeUnicode(res).strip()
                            if not path_isfile(coverfile):
                                logger.debug("Failed to create jpg: %s" % res)
                        except Exception as e:
                            logger.debug("Failed to create cover with %s [%s]" % (str(params), e))
                    else:
                        logger.warn("Failed to create jpg for %s" % issuefile)
            except Exception as e:
                logger.warn("Unable to create cover for %s using %s %s" % (issuefile, type(e).__name__, generator))
                logger.debug('Exception in create_cover: %s' % traceback.format_exc())

        if path_isfile(coverfile):
            setperm(coverfile)
            logger.debug("Created cover (page %d) for %s using %s" % (check_int(pagenum, 1), issuefile, generator))
            thumb = createthumb(coverfile)
            if thumb:
                logger.debug("Created thumbnail for cache")
                setperm(thumb)
                return thumb
            return coverfile

    # if not recognised extension or cover creation failed
    try:
        coverfile = safe_copy(os.path.join(lazylibrarian.PROG_DIR, 'data', 'images', 'nocover.jpg'), coverfile)
        setperm(coverfile)
        return coverfile
    except Exception as why:
        logger.error("Failed to copy nocover file, %s %s" % (type(why).__name__, str(why)))
    return ''
