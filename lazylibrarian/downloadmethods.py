#  This file is part of Lazylibrarian.
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.


import os
import re
import unicodedata
from base64 import b16encode, b32decode, b64encode
from hashlib import sha1

try:
    import urllib3
    import requests
except ImportError:
    import lib.requests as requests

# noinspection PyBroadException
try:
    # noinspection PyUnresolvedReferences
    import magic
except Exception:  # magic might fail for multiple reasons
    # noinspection PyBroadException
    try:
        import lib.magic as magic
    except Exception:
        magic = None

import lazylibrarian
from lazylibrarian import logger, database, nzbget, sabnzbd, classes, utorrent, transmission, qbittorrent, \
    deluge, rtorrent, synology
from lazylibrarian.cache import fetchURL
from lazylibrarian.common import setperm, getUserAgent, proxyList, make_dirs, namedic, \
    path_isdir, syspath, remove
from lazylibrarian.formatter import cleanName, unaccented, unaccented_bytes, getList, makeUnicode, md5_utf8, \
    seconds_to_midnight, replace_all, check_int
from lazylibrarian.postprocess import delete_task, check_contents
from lazylibrarian.providers import BlockProvider
from lazylibrarian.ircbot import ircConnect, ircSearch

try:
    from deluge_client import DelugeRPCClient
except ImportError:
    from lib.deluge_client import DelugeRPCClient
from .magnet2torrent import magnet2torrent
from lib.bencode import bencode, bdecode
from six import PY2, text_type

try:
    import html5lib
    from bs4 import BeautifulSoup
except ImportError:
    if PY2:
        from lib.bs4 import BeautifulSoup
    else:
        from lib3.bs4 import BeautifulSoup


def IrcDownloadMethod(bookid=None, dl_title=None, dl_url=None, library='eBook', provider=None):
    myDB = database.DBConnection()
    Source = provider
    msg = ''
    logger.debug("Starting IRC Download for [%s]" % dl_title)
    fname = ""
    data = None
    myprov = None

    for item in lazylibrarian.IRC_PROV:
        if item['NAME'] == provider or item['DISPNAME'] == provider:
            myprov = item
            break

    if not myprov:
        msg = "%s server not found" % provider
    else:
        irc = ircConnect(myprov)
        if not irc:
            msg = "Failed to connect"
            myprov['IRC'] = None
        else:
            fname, data = ircSearch(myprov, dl_title, cmd=':' + dl_url, cache=False)
            if not fname:
                myprov['IRC'] = None

    # noinspection PyTypeChecker
    downloadID = sha1(bencode(dl_url + ':' + dl_title)).hexdigest()

    if fname and data:
        fname = replace_all(fname, namedic)
        destdir = os.path.join(lazylibrarian.DIRECTORY('Download'), fname)
        if not path_isdir(destdir):
            _ = make_dirs(destdir)

        destfile = os.path.join(destdir, fname)

        try:
            with open(syspath(destfile), 'wb') as bookfile:
                bookfile.write(data)
            setperm(destfile)
        except Exception as e:
            msg = "%s writing book to %s, %s" % (type(e).__name__, destfile, e)
            logger.error(msg)
            return False, msg

        logger.debug('File %s has been downloaded from %s' % (dl_title, dl_url))
        if library == 'eBook':
            myDB.action('UPDATE books SET status="Snatched" WHERE BookID=?', (bookid,))
        elif library == 'AudioBook':
            myDB.action('UPDATE books SET audiostatus="Snatched" WHERE BookID=?', (bookid,))
        myDB.action('UPDATE wanted SET status="Snatched", Source=?, DownloadID=? WHERE NZBurl=? and NZBtitle=?',
                    (Source, downloadID, dl_url, dl_title))
        return True, ''

    elif not fname:
        msg = 'UPDATE wanted SET status="Failed", Source=?, DownloadID=?, DLResult=? '
        msg += 'WHERE NZBurl=? and NZBtitle=?'
        if not data:
            data = 'Failed'
        myDB.action(msg, (Source, downloadID, data, dl_url, dl_title))
        msg = data
        if 'timed out' in data:  # need to reconnect
            provider['IRC'] = None
            logger.error(msg)
    return False, msg


def NZBDownloadMethod(bookid=None, nzbtitle=None, nzburl=None, library='eBook'):
    myDB = database.DBConnection()
    Source = ''
    downloadID = ''

    if lazylibrarian.CONFIG['NZB_DOWNLOADER_SABNZBD'] and lazylibrarian.CONFIG['SAB_HOST']:
        Source = "SABNZBD"
        if lazylibrarian.CONFIG['SAB_EXTERNAL_HOST']:
            # new method, download nzb data, write to file, send file to sab, delete file
            data, success = fetchURL(nzburl, raw=True)
            if not success:
                res = 'Failed to read nzb data for sabnzbd: %s' % data
                logger.debug(res)
                downloadID = ''
            else:
                logger.debug("Got %s bytes data" % len(data))
                temp_filename = os.path.join(lazylibrarian.CACHEDIR, "nzbfile.nzb")
                with open(syspath(temp_filename), 'wb') as f:
                    f.write(data)
                logger.debug("Data written to file")
                nzb_url = lazylibrarian.CONFIG['SAB_EXTERNAL_HOST']
                if not nzb_url.startswith('http'):
                    if lazylibrarian.CONFIG['HTTPS_ENABLED']:
                        nzb_url = 'https://' + nzb_url
                    else:
                        nzb_url = 'http://' + nzb_url
                if lazylibrarian.CONFIG['HTTP_ROOT']:
                    nzb_url = nzb_url + '/' + lazylibrarian.CONFIG['HTTP_ROOT']
                nzb_url = nzb_url + '/nzbfile.nzb'
                logger.debug("nzb_url [%s]" % nzb_url)
                downloadID, res = sabnzbd.SABnzbd(nzbtitle, nzb_url, False)  # returns nzb_ids or False
                logger.debug("Sab returned %s/%s" % (downloadID, res))
                # os.unlink(temp_filename)
                # logger.debug("Temp file deleted")
        else:
            downloadID, res = sabnzbd.SABnzbd(nzbtitle, nzburl, False)  # returns nzb_ids or False

    if lazylibrarian.CONFIG['NZB_DOWNLOADER_NZBGET'] and lazylibrarian.CONFIG['NZBGET_HOST']:
        Source = "NZBGET"
        data, success = fetchURL(nzburl, raw=True)
        if not success:
            res = 'Failed to read nzb data for nzbget: %s' % data
            logger.debug(res)
            downloadID = ''
        else:
            nzb = classes.NZBDataSearchResult()
            nzb.extraInfo.append(data)
            nzb.name = nzbtitle
            nzb.url = nzburl
            downloadID, res = nzbget.sendNZB(nzb)

    if lazylibrarian.CONFIG['NZB_DOWNLOADER_SYNOLOGY'] and lazylibrarian.CONFIG['USE_SYNOLOGY'] and \
            lazylibrarian.CONFIG['SYNOLOGY_HOST']:
        Source = "SYNOLOGY_NZB"
        downloadID, res = synology.addTorrent(nzburl)  # returns nzb_ids or False

    if lazylibrarian.CONFIG['NZB_DOWNLOADER_BLACKHOLE']:
        Source = "BLACKHOLE"
        nzbfile, success = fetchURL(nzburl, raw=True)
        if not success:
            res = 'Error fetching nzb from url [%s]: %s' % (nzburl, nzbfile)
            logger.warn(res)
            return False, res

        if nzbfile:
            nzbname = str(nzbtitle) + '.nzb'
            nzbpath = os.path.join(lazylibrarian.CONFIG['NZB_BLACKHOLEDIR'], nzbname)
            try:
                with open(syspath(nzbpath), 'wb') as f:
                    if isinstance(nzbfile, text_type):
                        nzbfile = nzbfile.encode('iso-8859-1')
                    f.write(nzbfile)
                logger.debug('NZB file saved to: ' + nzbpath)
                setperm(nzbpath)
                downloadID = nzbname

            except Exception as e:
                res = '%s not writable, NZB not saved. %s: %s' % (nzbpath, type(e).__name__, str(e))
                logger.error(res)
                return False, res

    if not Source:
        res = 'No NZB download method is enabled, check config.'
        logger.warn(res)
        return False, res

    if downloadID:
        logger.debug('Nzbfile has been downloaded from ' + str(nzburl))
        if library == 'eBook':
            myDB.action('UPDATE books SET status="Snatched" WHERE BookID=?', (bookid,))
        elif library == 'AudioBook':
            myDB.action('UPDATE books SET audiostatus = "Snatched" WHERE BookID=?', (bookid,))
        myDB.action('UPDATE wanted SET status="Snatched", Source=?, DownloadID=? WHERE NZBurl=?',
                    (Source, downloadID, nzburl))
        return True, ''
    else:
        res = 'Failed to send nzb to @ <a href="%s">%s</a>' % (nzburl, Source)
        logger.error(res)
        return False, res


def DirectDownloadMethod(bookid=None, dl_title=None, dl_url=None, library='eBook', provider=''):
    myDB = database.DBConnection()
    Source = "DIRECT"
    logger.debug("Starting Direct Download for [%s]" % dl_title)
    proxies = proxyList()
    headers = {'Accept-encoding': 'gzip', 'User-Agent': getUserAgent()}
    dl_url = makeUnicode(dl_url)
    if provider == 'zlibrary':  # needs a referer header from a zlibrary host
        headers['Referer'] = dl_url
        if lazylibrarian.BOK_DLCOUNT >= check_int(lazylibrarian.CONFIG['BOK_DLLIMIT'], 5):
            res = 'Reached Daily download limit (%s)' % lazylibrarian.CONFIG['BOK_DLLIMIT']
            BlockProvider(provider, res, delay=seconds_to_midnight())
            return False, res

    redirects = 0
    while redirects < 5:
        redirects += 1
        try:
            logger.debug("%s: [%s] %s" % (redirects, provider, str(headers)))
            if not dl_url.startswith('http'):
                if headers.get('Referer', '').startswith('https://'):
                    dl_url = 'https://' + dl_url
                else:
                    dl_url = 'http://' + dl_url
            if dl_url.startswith('https') and lazylibrarian.CONFIG['SSL_VERIFY']:
                r = requests.get(dl_url, headers=headers, timeout=90,
                                 proxies=proxies,
                                 verify=lazylibrarian.CONFIG['SSL_CERTS'] if lazylibrarian.CONFIG[
                                     'SSL_CERTS'] else True)
            else:
                r = requests.get(dl_url, headers=headers, timeout=90, proxies=proxies, verify=False)
        except requests.exceptions.Timeout:
            res = 'Timeout fetching file from url: %s' % dl_url
            logger.warn(res)
            return False, res
        except Exception as e:
            res = '%s fetching file from url: %s, %s' % (type(e).__name__, dl_url, str(e))
            logger.warn(res)
            return False, res

        if not str(r.status_code).startswith('2'):
            res = "Got a %s response for %s" % (r.status_code, dl_url)
            logger.debug(res)
            return False, res
        elif len(r.content) < 1000:
            res = "Only got %s bytes for %s" % (len(r.content), dl_title)
            logger.debug(res)
            return False, res
        elif 'application' in r.headers['Content-Type']:
            # application/octet-stream, application/epub+zip, application/x-mobi8-ebook etc.
            extn = ''
            basename = ''
            if ' ' in dl_title:
                basename, extn = dl_title.rsplit(' ', 1)  # last word is often the extension - but not always...
            if extn and extn.lower() not in getList(lazylibrarian.CONFIG['EBOOK_TYPE']):
                basename = ''
                extn = ''
            if not basename and '.' in dl_title:
                basename, extn = dl_title.rsplit('.', 1)
            if extn and extn.lower() not in getList(lazylibrarian.CONFIG['EBOOK_TYPE']):
                basename = ''
                extn = ''
            if not basename and magic:
                try:
                    mtype = magic.from_buffer(r.content).upper()
                    logger.debug("magic reports %s" % mtype)
                except Exception as e:
                    logger.debug("%s reading magic from %s, %s" % (type(e).__name__, dl_title, e))
                    mtype = ''
                if 'EPUB' in mtype:
                    extn = 'epub'
                elif 'MOBIPOCKET' in mtype:  # also true for azw and azw3, does it matter?
                    extn = 'mobi'
                elif 'PDF' in mtype:
                    extn = 'pdf'
                elif 'RAR' in mtype:
                    extn = 'cbr'
                elif 'ZIP' in mtype:
                    extn = 'cbz'
                basename = dl_title
            if not extn:
                logger.warn("Don't know the filetype for %s" % dl_title)
                basename = dl_title
            if '/' in basename:
                basename = basename.split('/')[0]

            logger.debug("File download got %s bytes for %s" % (len(r.content), basename))
            if provider == 'zlibrary':
                lazylibrarian.BOK_DLCOUNT += 1

            basename = replace_all(basename, namedic)
            destdir = os.path.join(lazylibrarian.DIRECTORY('Download'), basename)
            if not path_isdir(destdir):
                _ = make_dirs(destdir)

            try:
                hashid = dl_url.split("md5=")[1].split("&")[0]
            except IndexError:
                # noinspection PyTypeChecker
                hashid = sha1(bencode(dl_url)).hexdigest()

            destfile = os.path.join(destdir, basename + '.' + extn)

            if os.name == 'nt':  # Windows has max path length of 256
                destfile = '\\\\?\\' + destfile

            try:
                with open(syspath(destfile), 'wb') as bookfile:
                    bookfile.write(r.content)
                setperm(destfile)
                downloadID = hashid
                logger.debug('File %s has been downloaded from %s' % (dl_title, dl_url))
                if library == 'eBook':
                    myDB.action('UPDATE books SET status="Snatched" WHERE BookID=?', (bookid,))
                elif library == 'AudioBook':
                    myDB.action('UPDATE books SET audiostatus="Snatched" WHERE BookID=?', (bookid,))
                myDB.action('UPDATE wanted SET status="Snatched", Source=?, DownloadID=? WHERE NZBurl=?',
                            (Source, downloadID, dl_url))
                return True, ''
            except Exception as e:
                res = "%s writing book to %s, %s" % (type(e).__name__, destfile, e)
                logger.error(res)
                return False, res
        else:
            res = "Got unexpected response type (%s) for %s" % (r.headers['Content-Type'], dl_title)
            logger.debug(res)
            if redirects and 'text/html' in r.headers['Content-Type'] and provider == 'zlibrary':
                host = lazylibrarian.CONFIG['BOK_HOST']
                headers['Referer'] = dl_url
                res, succ = fetchURL(r.url)
                if not succ:
                    return False, "Unable to fetch %s: %s" % (r.url, succ)
                try:
                    newsoup = BeautifulSoup(res, "html5lib")
                    a = newsoup.find('a', {"class": "dlButton"})
                    if not a:
                        link = ''
                        delay = 0
                        msg = ''
                        if 'WARNING' in res and '24 hours' in res:
                            msg = res.split('WARNING')[1].split('24 hours')[0]
                            msg = 'WARNING' + msg + '24 hours'
                            delay = seconds_to_midnight()
                        elif 'Too many requests' in res:
                            msg = res
                            delay = check_int(lazylibrarian.CONFIG['BLOCKLIST_TIMER'], 3600)
                        if delay:
                            BlockProvider(provider, msg, delay=delay)
                            logger.warn(msg)
                            return False, msg
                    else:
                        link = a.get('href')
                    if link and len(link) > 2:
                        dl_url = host + link
                    else:
                        return False, 'No link available'
                except Exception as e:
                    return False, "An error occurred parsing %s: %s" % (r.url, str(e))
            else:
                cacheLocation = os.path.join(lazylibrarian.CACHEDIR, "HTMLCache")
                myhash = md5_utf8(dl_url)
                hashfilename = os.path.join(cacheLocation, myhash[0], myhash[1], myhash + ".html")
                with open(syspath(hashfilename), "wb") as cachefile:
                    cachefile.write(r.content)
                logger.debug("Saved html page: %s" % hashfilename)
                return False, res

    res = 'Failed to download file @ <a href="%s">%s</a>' % (dl_url, dl_url)
    logger.error(res)
    return False, res


def TORDownloadMethod(bookid=None, tor_title=None, tor_url=None, library='eBook'):
    myDB = database.DBConnection()
    downloadID = False
    Source = ''
    torrent = ''

    full_url = tor_url  # keep the url as stored in "wanted" table
    tor_url = makeUnicode(tor_url)
    if 'magnet:?' in tor_url:
        # discard any other parameters and just use the magnet link
        tor_url = 'magnet:?' + tor_url.split('magnet:?')[1]
    else:
        # h = HTMLParser()
        # tor_url = h.unescape(tor_url)
        # HTMLParser is probably overkill, we only seem to get &amp;
        #
        tor_url = tor_url.replace('&amp;', '&')

        if '&file=' in tor_url:
            # torznab results need to be re-encoded
            # had a problem with torznab utf-8 encoded strings not matching
            # our utf-8 strings because of long/short form differences
            url, value = tor_url.split('&file=', 1)
            value = unicodedata.normalize('NFC', value)  # normalize to short form
            value = value.encode('unicode-escape')  # then escape the result
            value = makeUnicode(value)  # ensure unicode
            value = value.replace(' ', '%20')  # and encode any spaces
            tor_url = url + '&file=' + value

        # strip url back to the .torrent as some sites add extra parameters
        if not tor_url.endswith('.torrent') and '.torrent' in tor_url:
            tor_url = tor_url.split('.torrent')[0] + '.torrent'

        headers = {'Accept-encoding': 'gzip', 'User-Agent': getUserAgent()}
        proxies = proxyList()

        try:
            logger.debug("Fetching %s" % tor_url)
            if tor_url.startswith('https') and lazylibrarian.CONFIG['SSL_VERIFY']:
                r = requests.get(tor_url, headers=headers, timeout=90, proxies=proxies,
                                 verify=lazylibrarian.CONFIG['SSL_CERTS']
                                 if lazylibrarian.CONFIG['SSL_CERTS'] else True)
            else:
                r = requests.get(tor_url, headers=headers, timeout=90, proxies=proxies, verify=False)
            if str(r.status_code).startswith('2'):
                torrent = r.content
                if not len(torrent):
                    res = "Got empty response for %s" % tor_url
                    logger.warn(res)
                    return False, res
                elif len(torrent) < 100:
                    res = "Only got %s bytes for %s" % (len(torrent), tor_url)
                    logger.warn(res)
                    return False, res
                else:
                    logger.debug("Got %s bytes for %s" % (len(torrent), tor_url))
            else:
                res = "Got a %s response for %s" % (r.status_code, tor_url)
                logger.warn(res)
                return False, res

        except requests.exceptions.Timeout:
            res = 'Timeout fetching file from url: %s' % tor_url
            logger.warn(res)
            return False, res
        except Exception as e:
            # some jackett providers redirect internally using http 301 to a magnet link
            # which requests can't handle, so throws an exception
            logger.debug("Requests exception: %s" % str(e))
            if "magnet:?" in str(e):
                tor_url = 'magnet:?' + str(e).split('magnet:?')[1].strip("'")
                logger.debug("Redirecting to %s" % tor_url)
            else:
                res = '%s fetching file from url: %s, %s' % (type(e).__name__, tor_url, str(e))
                logger.warn(res)
                return False, res

    if not torrent and not tor_url.startswith('magnet:?'):
        res = "No magnet or data, cannot continue"
        logger.warn(res)
        return False, res

    if lazylibrarian.CONFIG['TOR_DOWNLOADER_BLACKHOLE']:
        Source = "BLACKHOLE"
        logger.debug("Sending %s to blackhole" % tor_title)
        tor_name = cleanName(tor_title).replace(' ', '_')
        if tor_url and tor_url.startswith('magnet'):
            hashid = calculate_torrent_hash(tor_url)
            if not hashid:
                hashid = tor_name
            if lazylibrarian.CONFIG['TOR_CONVERT_MAGNET']:
                tor_name = 'meta-' + hashid + '.torrent'
                tor_path = os.path.join(lazylibrarian.CONFIG['TORRENT_DIR'], tor_name)
                result = magnet2torrent(tor_url, tor_path)
                if result is not False:
                    logger.debug('Magnet file saved as: %s' % tor_path)
                    downloadID = hashid
            else:
                tor_name += '.magnet'
                tor_path = os.path.join(lazylibrarian.CONFIG['TORRENT_DIR'], tor_name)
                msg = ''
                try:
                    msg = 'Opening '
                    with open(syspath(tor_path), 'wb') as torrent_file:
                        msg += 'Writing '
                        if isinstance(torrent, text_type):
                            torrent = torrent.encode('iso-8859-1')
                        torrent_file.write(torrent)
                    msg += 'SettingPerm '
                    setperm(tor_path)
                    msg += 'Saved '
                    logger.debug('Magnet file saved: %s' % tor_path)
                    downloadID = hashid
                except Exception as e:
                    res = "Failed to write magnet to file: %s %s" % (type(e).__name__, str(e))
                    logger.warn(res)
                    logger.debug("Progress: %s Filename [%s]" % (msg, repr(tor_path)))
                    return False, res
        else:
            tor_name += '.torrent'
            tor_path = os.path.join(lazylibrarian.CONFIG['TORRENT_DIR'], tor_name)
            msg = ''
            try:
                msg = 'Opening '
                with open(syspath(tor_path), 'wb') as torrent_file:
                    msg += 'Writing '
                    if isinstance(torrent, text_type):
                        torrent = torrent.encode('iso-8859-1')
                    torrent_file.write(torrent)
                msg += 'SettingPerm '
                setperm(tor_path)
                msg += 'Saved '
                logger.debug('Torrent file saved: %s' % tor_name)
                downloadID = Source
            except Exception as e:
                res = "Failed to write torrent to file: %s %s" % (type(e).__name__, str(e))
                logger.warn(res)
                logger.debug("Progress: %s Filename [%s]" % (msg, repr(tor_path)))
                return False, res

    else:
        hashid = calculate_torrent_hash(tor_url, torrent)
        if not hashid:
            res = "Unable to calculate torrent hash from url/data"
            logger.error(res)
            logger.debug("url: %s" % tor_url)
            logger.debug("data: %s" % makeUnicode(str(torrent[:50])))
            return False, res

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_UTORRENT'] and lazylibrarian.CONFIG['UTORRENT_HOST']:
            logger.debug("Sending %s to Utorrent" % tor_title)
            Source = "UTORRENT"
            downloadID, res = utorrent.addTorrent(tor_url, hashid)  # returns hash or False
            if downloadID:
                tor_title = utorrent.nameTorrent(downloadID)

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_RTORRENT'] and lazylibrarian.CONFIG['RTORRENT_HOST']:
            logger.debug("Sending %s to rTorrent" % tor_title)
            Source = "RTORRENT"
            if not torrent and tor_url.startswith('magnet:?'):
                logger.debug("Converting magnet to data for rTorrent")
                torrentfile = magnet2torrent(tor_url)
                if torrentfile:
                    with open(syspath(torrentfile), 'rb') as f:
                        torrent = f.read()
                    remove(torrentfile)
                if not torrent:
                    logger.debug("Unable to convert magnet")
            if torrent:
                logger.debug("Sending %s data to rTorrent" % tor_title)
                downloadID, res = rtorrent.addTorrent(tor_title, hashid, data=torrent)
            else:
                logger.debug("Sending %s url to rTorrent" % tor_title)
                downloadID, res = rtorrent.addTorrent(tor_url, hashid)  # returns hash or False
            if downloadID:
                tor_title = rtorrent.getName(downloadID)

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_QBITTORRENT'] and lazylibrarian.CONFIG['QBITTORRENT_HOST']:
            Source = "QBITTORRENT"
            logger.debug("Sending %s url to qBittorrent" % tor_title)
            status, res = qbittorrent.addTorrent(tor_url, hashid)  # returns True or False
            if status:
                downloadID = hashid
                tor_title = qbittorrent.getName(hashid)

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_TRANSMISSION'] and lazylibrarian.CONFIG['TRANSMISSION_HOST']:
            Source = "TRANSMISSION"
            if torrent:
                logger.debug("Sending %s data to Transmission" % tor_title)
                # transmission needs b64encoded metainfo to be unicode, not bytes
                downloadID, res = transmission.addTorrent(None, metainfo=makeUnicode(b64encode(torrent)))
            else:
                logger.debug("Sending %s url to Transmission" % tor_title)
                downloadID, res = transmission.addTorrent(tor_url)  # returns id or False
            if downloadID:
                # transmission returns it's own int, but we store hashid instead
                downloadID = hashid
                tor_title = transmission.getTorrentFolder(downloadID)

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_SYNOLOGY'] and lazylibrarian.CONFIG['USE_SYNOLOGY'] and \
                lazylibrarian.CONFIG['SYNOLOGY_HOST']:
            logger.debug("Sending %s url to Synology" % tor_title)
            Source = "SYNOLOGY_TOR"
            downloadID, res = synology.addTorrent(tor_url)  # returns id or False
            if downloadID:
                tor_title = synology.getName(downloadID)

        if lazylibrarian.CONFIG['TOR_DOWNLOADER_DELUGE'] and lazylibrarian.CONFIG['DELUGE_HOST']:
            if not lazylibrarian.CONFIG['DELUGE_USER']:
                # no username, talk to the webui
                Source = "DELUGEWEBUI"
                if torrent:
                    logger.debug("Sending %s data to Deluge" % tor_title)
                    downloadID, res = deluge.addTorrent(tor_title, data=b64encode(torrent))
                else:
                    logger.debug("Sending %s url to Deluge" % tor_title)
                    downloadID, res = deluge.addTorrent(tor_url)  # can be link or magnet, returns hash or False
                if downloadID:
                    tor_title = deluge.getTorrentFolder(downloadID)
                else:
                    return False, res
            else:
                # have username, talk to the daemon
                Source = "DELUGERPC"
                client = DelugeRPCClient(lazylibrarian.CONFIG['DELUGE_HOST'],
                                         int(lazylibrarian.CONFIG['DELUGE_PORT']),
                                         lazylibrarian.CONFIG['DELUGE_USER'],
                                         lazylibrarian.CONFIG['DELUGE_PASS'],
                                         decode_utf8=True)
                try:
                    client.connect()
                    args = {"name": tor_title}
                    if tor_url.startswith('magnet'):
                        res = "Sending %s magnet to DelugeRPC" % tor_title
                        logger.debug(res)
                        downloadID = client.call('core.add_torrent_magnet', tor_url, args)
                    elif torrent:
                        res = "Sending %s data to DelugeRPC" % tor_title
                        logger.debug(res)
                        downloadID = client.call('core.add_torrent_file', tor_title,
                                                 b64encode(torrent), args)
                    else:
                        res = "Sending %s url to DelugeRPC" % tor_title
                        logger.debug(res)
                        downloadID = client.call('core.add_torrent_url', tor_url, args)
                    if downloadID:
                        if lazylibrarian.CONFIG['DELUGE_LABEL']:
                            _ = client.call('label.set_torrent', downloadID,
                                            lazylibrarian.CONFIG['DELUGE_LABEL'].lower())
                        result = client.call('core.get_torrent_status', downloadID, {})
                        if 'name' in result:
                            tor_title = result['name']
                    else:
                        res += ' failed'
                        logger.error(res)
                        return False, res

                except Exception as e:
                    res = 'DelugeRPC failed %s %s' % (type(e).__name__, str(e))
                    logger.error(res)
                    return False, res

    if not Source:
        res = 'No torrent download method is enabled, check config.'
        logger.warn(res)
        return False, res

    if downloadID:
        if tor_title:
            if makeUnicode(downloadID).upper() in makeUnicode(tor_title).upper():
                logger.warn('%s: name contains hash, probably unresolved magnet' % Source)
            else:
                if PY2:
                    tor_title = unaccented_bytes(tor_title, only_ascii=False)
                else:
                    tor_title = unaccented(tor_title, only_ascii=False)
                # need to check against reject words list again as the name may have changed
                # library = magazine eBook AudioBook to determine which reject list
                # but we can't easily do the per-magazine rejects
                if library == 'magazine':
                    reject_list = getList(lazylibrarian.CONFIG['REJECT_MAGS'], ',')
                elif library == 'eBook':
                    reject_list = getList(lazylibrarian.CONFIG['REJECT_WORDS'], ',')
                elif library == 'AudioBook':
                    reject_list = getList(lazylibrarian.CONFIG['REJECT_AUDIO'], ',')
                elif library == 'Comic':
                    reject_list = getList(lazylibrarian.CONFIG['REJECT_COMIC'], ',')
                else:
                    logger.debug("Invalid library [%s] in TORDownloadMethod" % library)
                    reject_list = []

                rejected = False
                lower_title = tor_title.lower()
                for word in reject_list:
                    if word in lower_title:
                        rejected = "Rejecting torrent name %s, contains %s" % (tor_title, word)
                        logger.debug(rejected)
                        break
                if not rejected:
                    rejected = check_contents(Source, downloadID, library, tor_title)
                if rejected:
                    myDB.action('UPDATE wanted SET status="Failed",DLResult=? WHERE NZBurl=?',
                                (rejected, full_url))
                    if lazylibrarian.CONFIG['DEL_FAILED']:
                        delete_task(Source, downloadID, True)
                    return False, rejected
                else:
                    logger.debug('%s setting torrent name to [%s]' % (Source, tor_title))
                    myDB.action('UPDATE wanted SET NZBtitle=? WHERE NZBurl=?', (tor_title, full_url))

        if library == 'eBook':
            myDB.action('UPDATE books SET status="Snatched" WHERE BookID=?', (bookid,))
        elif library == 'AudioBook':
            myDB.action('UPDATE books SET audiostatus="Snatched" WHERE BookID=?', (bookid,))
        myDB.action('UPDATE wanted SET status="Snatched", Source=?, DownloadID=? WHERE NZBurl=?',
                    (Source, downloadID, full_url))
        return True, ''

    res = 'Failed to send torrent to %s' % Source
    logger.error(res)
    return False, res


def calculate_torrent_hash(link, data=None):
    """
    Calculate the torrent hash from a magnet link or data. Returns empty string
    when it cannot create a torrent hash given the input data.
    """
    try:
        torrent_hash = re.findall(r"urn:btih:([\w]{32,40})", link)[0]
        if len(torrent_hash) == 32:
            torrent_hash = b16encode(b32decode(torrent_hash)).lower()
    except (re.error, IndexError, TypeError):
        if data:
            try:
                # noinspection PyUnresolvedReferences
                info = bdecode(data)["info"]
                # noinspection PyTypeChecker
                torrent_hash = sha1(bencode(info)).hexdigest()
            except Exception as e:
                logger.error("Error calculating hash: %s" % e)
                return ''
        else:
            logger.error("Cannot calculate torrent hash without magnet link or data")
            return ''
    logger.debug('Torrent Hash: %s' % str(torrent_hash))
    return torrent_hash
